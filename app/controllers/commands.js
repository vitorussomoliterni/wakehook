"use strict";

const exec = require('child_process').exec;

exports.wakeMachine = (wakeCommand, machineAddress) => {
  const command = `${wakeCommand} ${machineAddress}`;
  exec(
    command,
    (err, stdout, stderr) => {
      const message = stdout || err.message;
      console.log(message);
      return;
    }
  );
};