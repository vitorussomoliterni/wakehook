"use strict";

exports.mapMachines = machines =>
  machines
    .split(',')
    .map(c => c.split('|'))
    .map(c => {
      return {name : c[0] || '' , address : c[1] || '' } 
  });

exports.getMacAddress = (machines, machineName) => {
  if (!machineName || !machines) {
    return;
  }

  const machine = machines.find(c => c.name == machineName);
  
  if (!machine || !machine.address) {
    return;
  }

  return machine.address;
};