"use strict";

const express = require('express');
const dotenv = require('dotenv');
const machineController = require('./controllers/machines');
const commandsController = require('./controllers/commands');
const helmet = require('helmet');
const compression = require('compression');

const app = express();
app.use(helmet()).use(compression());

// Importing the modules from the controllers
const mapMachines = machineController.mapMachines;
const getMacAddress = machineController.getMacAddress;
const wakeMachine = commandsController.wakeMachine;

// Getting the configs
dotenv.config();
const port = process.env.PORT || 3000;
const machines = process.env.COMPUTERS ? mapMachines(process.env.COMPUTERS) : {};
const wakeCommand = process.env.WAKE_COMMAND || 'wakeonlan';
const passphrase = process.env.PASSPHRASE;

// GET: wake?machine=hal&passphrase=2001spaceodissey
app.get('/wake', (req, res) =>
  {
    if (req.query.passphrase == passphrase && req.query.machine) {
      const address = getMacAddress(machines, req.query.machine);
      if (!address) {
        res.sendStatus(404); // Respond 404 if no mac address was found
        return;
      }
      wakeMachine(wakeCommand, address);
      res.sendStatus(200);
    } else {
      res.sendStatus(403); // Respond 403 if no valid machinename or passphrase provided
    }
  }
);

app.listen(port, () => console.log(`Wakehook listening on port ${port}`))
